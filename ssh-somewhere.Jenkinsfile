pipeline {
    agent { kubernetes { yamlFile "default-shell.yaml" } }

    parameters {
      string( name: 'SLEEP', description: "How long to sleep")
      file( name: 'FILE', description: "What file to copy")
      choice(
        name: 'DB_TYPE',
        description: 'Database',
        choices: ["oracle", "cockroach"]
      )
    }

    environment {
        SSH_MARKETVIEW_ROCKSTAR=credentials("marketview-at-rockstar")
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '10'))
        disableConcurrentBuilds()
        timeout(time: 1, unit: 'HOURS')
        timestamps()
    }

    stages {
        stage('Prechecks') {
            steps {
                script {
                    if (params.SLEEP.isEmpty()) {
                        echo "Parameter SLEEP cannot be empty"
                        currentBuild.result = 'ABORTED'
                        error("Aborting the build")
                    }
                    if ("${FILE}" == "") {
                        echo "Parameter FILE cannot be empty"
                        currentBuild.result = 'ABORTED'
                        error("Aborting the build")
                    }
                    if (params.DB_TYPE.isEmpty()) {
                        echo "Parameter DB_TYPE cannot be empty"
                        currentBuild.result = 'ABORTED'
                        error("Aborting the build")
                    }
                }
            }
        }
        stage('Execute') {
            steps {
                lock(resource: 'rockstar') {
                    script {
                        currentBuild.displayName = "#${BUILD_NUMBER} sleep ${params.SLEEP}"
                        currentBuild.description = "Copy ${FILE}"

                        sh("/bin/bash -xe ssh-somewhere.sh")
                    }
                }
            }
        }
    }
}
